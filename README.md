<div align="center">
  <large>/ ! \ Ce projet a été déplacé sur [l'instance git de l'UGA](https://gricad-gitlab.univ-grenoble-alpes.fr/elan) /!\</large>
  <img src="application/public/img/Jean-Philippe_Toussaint,_Florence_(Italie),_2013.JPG" width="128">
  <h1>Projet "Réticence"</h1>
</div>

## Installation
```
git clone https://gitlab.com/litt-arts-num/toussaint.git
cd toussaint
make init
```
puis importer les données : ouvrir la console de docker `docker-compose exec apache bash` et lancer la commande
```
bin/console app:import
```
Cela importe les données issues du TEICorpus : application/data/tei/toussaint_tei_corpus.xml

## Documentation
### Données
Les données du corpus sont dans le dossier `application/data`.

#### TEI
**_à compléter_**


Les données prennent la forme d'un TEICorpus. Celui-ci permet d'inclure l'ensemble des TEI pour chaque feuillet du corpus. Ainsi, chaque feuillet dispose d'une transcription "autonome" et conforme à la TEI et l'ensemble du corpus est aussi réunit au sein d'un unique du fichier XML.

**Attention avec PHP** : le teiCorpus utilise xi:include (cf. XXX) ainsi pour une bonne manipulation du XML par XMLDom de php, il faut penser à utiliser la méthode `xinclude()`. Voici un exemple minimal complet en supposant que `$xslFile` et `$xmlFile` contiennent respectivement les chemins vers les fichiers XSL et XML :
```php
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

	$xslt = new \XSLTProcessor();
	$xslt->importStylesheet($xsl);

	$xml = new \DOMDocument();
	$xml->load($xmlFile);
	/* Mandatory with xi:include
	   Option LIBXML_NOWARNING is useful in our case: indeed, we prepared xi:include for future transcription: thus the included file is not always available. In this case, the XSL will return a warning */
	$xml->xinclude(LIBXML_NOWARNING);

	/* set your parameters */
	//$xslt->setParameter('', 'pagenum', $pageName);
	//$xslt->setParameter('', 'ask', $what);
	//$xslt->setParameter('', 'standalone', "0");

	/* get the result */
	$xml = $xslt->transformToXML($xml);
```

#### XSLT
Les feuilles de transformation sont dans le dossier `application/data/xslt`. Elles suivent les décisions éditoriales prises au cours du projet.

Les XSL sont organisés ainsi :
  * `LRT_tei2html.xsl` : feuille de transformation principale, incluant les autres
  * `LRT_tei2tapuscrit.xsl` et `LRT_tei2final.xsl` : contiennent des templates spécifiques à certains rendus

Utilisation :
`LRT_tei2html.xsl` est prévu pour traiter le fichier `toussaint_tei_corpus.xml` (XML-TEI / teiCorpus) et doit être utilisé avec les paramètres suivants :
  * `ask` qui peut prendre l'une des valeurs suivantes : `tapuscrit`, `final`, `img`, `list_available_xml` ou `list_all_leaflet`
  * `pagenum`, paramètre obligatoire si `ask` prend l'une des valeurs `tapuscrit`, `final` ou `img`, doit correspondre à l'identifiant d'un feuillet, c'est-à-dire le nom du fichier correspondant au facsimilé sans extension. Par exemple, "LRT_02_01_00301".
  * `standalone` dont la valeur par défaut est 0. S'il prend la valeur 1, la sortie incluera des entêtes HTML minimaux permettant un affichage des contenus

Sortie :
  * `ask=tapuscrit` renvoi du code html permettant de voir la transcription correspondant uniquement au tapuscrit du feuillet désigné par le paramètre `pagenum`
  * `ask=final` renvoi du code html permettant de voir la transcription ainsi que l'ensemble des modifications manuscrites du feuillet désigné par le paramètre `pagenum`
  * `ask=img` renvoi le nom du complet du fichier correspondant au facsimilé du feuillet désigné par le paramètre `pagenum`
  * `ask=list_available_xml` renvoie la liste des feuillets pour lesquels une transcription est disponible (un feuillet par ligne)
  * `ask=list_all_leaflet` renvoie la liste des feuillets disponibles (un feuillet par ligne)

## À propos
[demarreshs.hypotheses.org](https://demarreshs.hypotheses.org/projet-reticence-manuscrits-de-jean-philippe-toussaint)

[jptoussaint.com](http://www.jptoussaint.com/projet-reticence.html)

## Crédits
<a href="https://commons.wikimedia.org/wiki/File:Jean-Philippe_Toussaint,_Florence_(Italie),_2013.JPG">Image By Madeleinesantandrea [CC BY-SA 3.0], from Wikimedia Commons</a>
