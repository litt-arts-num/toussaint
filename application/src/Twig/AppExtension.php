<?php

namespace App\Twig;

use App\Entity\Cahier;
use App\Entity\Fiche;
use App\Manager\XmlManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Doctrine\ORM\EntityManagerInterface;

class AppExtension extends AbstractExtension
{
    public function __construct(EntityManagerInterface $em, XmlManager $xmlManager)
    {
        $this->em = $em;
        $this->xmlManager = $xmlManager;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getNext', [$this, 'getNext']),
            new TwigFunction('getHTML', [$this, 'getHTML']),
        ];
    }

    public function getNext(Fiche $fiche)
    {
        return $this->em->getRepository(Fiche::class)->findOneByPrevious($fiche);
    }

    public function getHTML($what, $pageName)
    {
        return $this->xmlManager->getHTML($what, $pageName);
    }
}
