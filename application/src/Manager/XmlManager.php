<?php

namespace App\Manager;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class XmlManager
{
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->dir = $this->params->get('data_directory');
    }

    public function getHTML($what, $pageName)
    {
        $xslFile = $this->dir.'xslt/LRT_tei2html.xsl';
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

        $xslt = new \XSLTProcessor();
        $xslt->importStylesheet($xsl);

        $xmlFile = $this->dir.'tei/toussaint_tei_corpus.xml';
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        $xml->xinclude(LIBXML_NOWARNING);

        $xslt->setParameter('', 'pagenum', $pageName);
        $xslt->setParameter('', 'ask', $what);
        $xslt->setParameter('', 'standalone', "0");
        $xml = $xslt->transformToXML($xml);

        return $xml;
    }
}
