<?php

namespace App\Command;

use App\Entity\Fiche;
use App\Entity\Boite;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;

class ImportCommand extends Command
{
    private $em;
    protected static $defaultName = 'app:import';

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('import');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $xml = file_get_contents('data/tei/toussaint_tei_corpus.xml');
        $crawler = new Crawler($xml);

        $fiches = $crawler->filter('xi|include')->each(function (Crawler $node, $i) use ($crawler) {
            $xmlPath = $node->attr('href');
            $name = substr($xmlPath, strrpos($xmlPath, '/') + 1, -4);
            // $imageURL = $crawler->filter("graphic[xml|id='$imageID']")->attr('url');
            // voir import manager HDR $thumbnailURL = $this->xmlManager->getHTML("thumbnail", $cahier, $pageName);
            $imageURL = $name.".jpg";
            $infos = explode("_", $xmlPath);
            $boiteName = $infos[1];
            $pochetteName = $infos[2];

            return [$name, $boiteName, $pochetteName, $imageURL];
        });

        $previous = null;
        $boites = [];
        foreach ($fiches as $f) {
            $name = $f[0];
            $boiteName = $f[1];
            $pochetteName = $f[2];

            if (!isset($boites[$boiteName])) {
                if (!$boite = $this->em->getRepository(Boite::class)->findOneByName($boiteName)) {
                    $boite = new Boite;
                    $boite->setName($boiteName);
                    $this->em->persist($boite);
                }
                $boites[$boiteName] = $boite;
            } else {
                $boite = $boites[$boiteName];
            }

            if (!$fiche = $this->em->getRepository(Fiche::class)->findOneBy(["name" => $name, "boite" => $boite])) {
                $fiche = new Fiche;
                $fiche->setPrevious($previous);
                $fiche->setName($name);
                $fiche->setImage($f[3]);
                $fiche->setBoite($boite);
                $fiche->setHasContent(true);
                $previous = $fiche;

                $this->em->persist($fiche);
            }
        }
        $this->em->flush();

        $io->success('import done');
    }
}
