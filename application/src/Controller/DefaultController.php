<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Boite;
use App\Entity\Fiche;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/le-projet", name="project")
     */
    public function project()
    {
        return $this->render('default/project.html.twig');
    }

    /**
     * @Route("/a-propos", name="about")
     */
    public function about()
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/credits", name="credits")
     */
    public function credits()
    {
        return $this->render('default/credits.html.twig');
    }

    /**
     * @Route("/inventaire", name="inventory")
     */
    public function inventory()
    {
        return $this->render('default/inventory.html.twig');
    }

    /**
     * @Route("/creations", name="creations")
     */
    public function creations()
    {
        return $this->render('default/creations.html.twig');
    }

    /**
     * @Route("/boites", name="boites")
     */
    public function listBoites()
    {
        $boites = $this->getDoctrine()->getRepository(Boite::class)->findAll();

        return $this->render('default/boites.html.twig', ["boites" => $boites]);
    }

    /**
     * @Route("/boite/{boite}/fiches", name="boite_fiches")
     */
    public function listFiches($boite)
    {
        $boite = $this->getDoctrine()->getRepository(Boite::class)->findOneByName($boite);
        $fiches = $this->getDoctrine()->getRepository(Fiche::class)->findByBoite($boite);

        return $this->render('default/fiches.html.twig', ["boite" => $boite, "fiches" => $fiches]);
    }

    /**
     * @Route("/boite/{boite}/{fiche}", name="display_fiche")
     */
    public function displayFiche($boite, $fiche)
    {
        $boite = $this->getDoctrine()->getRepository(Boite::class)->findOneByName($boite);
        $fiche = $this->getDoctrine()->getRepository(Fiche::class)->findOneBy(["boite" => $boite, "name" => $fiche]);

        return $this->render('default/fiche.html.twig', ["fiche" => $fiche]);
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }

    /**
     * @Route("/experimentations", name="experimentations")
     */
    public function experimentations()
    {
        return $this->render('default/experimentations.html.twig');
    }

    /**
     * @Route("/experimentation/steps-natures", name="experimentation_stepsnature")
     */
    public function experimentation1()
    {
        return $this->render('vincent/steps_natures/index.html.twig');
    }

    /**
     * @Route("/experimentation/steps", name="experimentation_steps")
     */
    public function experimentation2()
    {
        return $this->render('vincent/steps/index.html.twig');
    }

    /**
     * @Route("/experimentation/graph", name="experimentation_graph")
     */
    public function experimentation3()
    {
        return $this->render('vincent/graph/index.html.twig');
    }

    /**
     * @Route("/experimentation/loupe", name="experimentation_loupe")
     */
    public function experimentation4()
    {
        return $this->render('vincent/loupe/index.html.twig');
    }

    /**
     * @Route("/experimentation/range", name="experimentation_range")
     */
    public function experimentation5()
    {
        return $this->render('vincent/range/index.html.twig');
    }
}
