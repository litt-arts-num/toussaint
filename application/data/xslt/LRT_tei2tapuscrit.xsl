<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">

    <xsl:strip-space elements="*"/>

    
    <xsl:template match="/tei:TEI" mode="tapuscrit">
        <xsl:apply-templates select=".//tei:body" mode="tapuscrit"/>
    </xsl:template>
    
    <xsl:template match="tei:fw" mode="tapuscrit">
        <xsl:choose>
            <xsl:when test="@type = 'pageNum'">
                <p class="page-num">
                    <xsl:value-of select="."/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <mark class="warning">
                    <xsl:attribute name="title">
                        <xsl:text>Unknown type of fw : "</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:text>"</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="tei:div" mode="tapuscrit">
        <xsl:apply-templates mode="tapuscrit"/>
    </xsl:template>
    
    <xsl:template match="tei:p" mode="tapuscrit">
        <p class="content">
            <xsl:apply-templates mode="tapuscrit"/>
        </p>
    </xsl:template>
    
    <xsl:template match="tei:lb" mode="tapuscrit">
        <br/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="tei:persName" mode="tapuscrit">
        <mark class="info name" title="Nom de personne">
            <xsl:apply-templates mode="tapuscrit"/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:name" mode="tapuscrit">
        <mark class="info name">
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@type = 'place'">
                        <xsl:text>Nom de lieu</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type = 'pers'">
                        <xsl:text>Nom de personnage</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Nom de "</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:text>"</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates mode="tapuscrit"/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:rs" mode="tapuscrit">
        <mark class="info refering_string">
            <xsl:attribute name="title">
                <xsl:text>Référence au personnage </xsl:text>
                <xsl:value-of select="@type"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:add" mode="tapuscrit"/>
    
    <xsl:template match="tei:del" mode="tapuscrit">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:subst" mode="tapuscrit">
        <xsl:apply-templates select="tei:del" mode="tapuscrit"/>
    </xsl:template>
    
    <xsl:template match="tei:unclear" mode="tapuscrit">
        <xsl:choose>
            <xsl:when test="text()">
                <mark title="Peu lisible">
                    <xsl:value-of select="."/>
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <mark title="Peu lisible">
                    ???
                </mark>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>