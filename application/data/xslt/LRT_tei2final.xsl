<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">
    
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/tei:TEI" mode="final">
        <xsl:apply-templates select=".//tei:body" mode="final"/>
    </xsl:template>
    
    <xsl:template match="tei:fw" mode="final">
        <xsl:choose>
            <xsl:when test="@type = 'pageNum'">
                <p class="page-num">
                    <xsl:value-of select="."/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <mark class="warning">
                    <xsl:attribute name="title">
                        <xsl:text>Unknown type of fw : "</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:text>"</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="tei:div" mode="final">
        <xsl:apply-templates mode="final"/>
    </xsl:template>
    
    <xsl:template match="tei:p" mode="final">
        <p class="content">
            <xsl:apply-templates mode="final"/>
        </p>
    </xsl:template>
    
    <xsl:template match="tei:lb" mode="final">
        <br/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="tei:persName" mode="final">
        <mark class="info name" title="Nom de personne">
            <xsl:apply-templates mode="final"/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:name" mode="final">
        <mark class="info name">
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@type = 'place'">
                        <xsl:text>Nom de lieu</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type = 'pers'">
                        <xsl:text>Nom de personnage</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Nom de "</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:text>"</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates mode="final"/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:rs" mode="final">
        <mark class="info refering_string">
            <xsl:attribute name="title">
                <xsl:text>Référence au personnage </xsl:text>
                <xsl:value-of select="@type"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
            <span class="fa-info">*</span>
        </mark>
    </xsl:template>
    
    <xsl:template match="tei:add" mode="final">
        <xsl:param name="title">ajout</xsl:param>
        <span class="manuscrit add" title="{$title}"><xsl:apply-templates mode="final"/></span>
    </xsl:template>
    
    <xsl:template match="tei:del" mode="final">
        <xsl:param name="title">suppression</xsl:param>
        <span class="del manuscrit" title="{$title}">
            <xsl:attribute name="class">
                <xsl:text>del </xsl:text>
                <xsl:value-of select="@rend"/>
            </xsl:attribute>
            <xsl:apply-templates mode="final"/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:subst" mode="final">
        <span class="subst"  title="substitution">
            <span>
                <xsl:attribute name="class">
                    <xsl:text>subst del </xsl:text>
                    <xsl:value-of select="@rend"/>
                </xsl:attribute>
                <xsl:apply-templates select="tei:del" mode="final">
                    <xsl:with-param name="title">substitution</xsl:with-param>
                </xsl:apply-templates>
            </span>
            <span class="subst add manuscrit">
                <xsl:apply-templates select="tei:add" mode="final">
                    <xsl:with-param name="title">substitution</xsl:with-param>
                </xsl:apply-templates>
            </span>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:unclear" mode="final">
        <xsl:choose>
            <xsl:when test="text()">
                <mark class="unclear" title="Peu lisible">
                    <xsl:value-of select="."/>
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <mark class="unclear"  title="Peu lisible">
                    <xsl:text>???</xsl:text>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
