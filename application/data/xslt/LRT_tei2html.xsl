<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs tei xi" version="1.0">
    
    <xsl:output method="html" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:strip-space elements="*"/>
    <xsl:strip-space elements="tei:*"/>
    
    <xsl:variable name="newline">
        <xsl:text>
</xsl:text>
    </xsl:variable>
    
    <xsl:include href="LRT_tei2tapuscrit.xsl"/>
    <xsl:include href="LRT_tei2final.xsl"/>
    
    <xsl:param name="standalone">0</xsl:param>
    <!--
        <xsl:param name="ask">tapuscrit</xsl:param>
        <xsl:param name="ask">final</xsl:param>
        <xsl:param name="ask">img</xsl:param>
        <xsl:param name="ask">list_available_xml</xsl:param>
        <xsl:param name="ask">count_available_xml</xsl:param>
        <xsl:param name="ask">list_all_leaflet</xsl:param>
        <xsl:param name="ask">is_xml_available</xsl:param>
        <xsl:param name="ask">title</xsl:param>
    -->
    <xsl:param name="ask">title</xsl:param>
    <xsl:param name="pagenum">LRT_02_01_00301</xsl:param>
    <!--<xsl:param name="pagenum">LRT_01_01_00001</xsl:param>-->
    <xsl:param name="boxnum">02</xsl:param>
    <!--<xsl:param name="boxnum">all</xsl:param>-->
    <xsl:param name="current_mode">
        <xsl:value-of select="$ask"/>
    </xsl:param>
    
    <xsl:template match="/">
        <xsl:if test="
            $standalone = 1
            and $ask != 'list_available_xml'
            and $ask != 'list_all_leaflet'
            and $ask != 'count_available_xml'
            and $ask != 'is_xml_available'
            and $ask != 'title'
            ">
            <xsl:text disable-output-escaping="yes">
&lt;!DOCTYPE html>
&lt;html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
  &lt;head>
    &lt;meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    &lt;link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    &lt;script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">&lt;/script>
    &lt;script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">&lt;/script>
    &lt;script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">&lt;/script>
    &lt;link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous"/>
    &lt;link rel="stylesheet" href="../../assets/css/reticence.css"/>
&lt;/head>
  &lt;body>
</xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="$current_mode = 'tapuscrit'">
                <xsl:choose>
                    <xsl:when
                        test="count(/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]) > 0">
                        <div class="transcription tapuscrit m-4 col-11">
                            <xsl:apply-templates mode="tapuscrit"
                                select="/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]/tei:text/tei:body"
                            />
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <p class="info">Cette fiche n'est pas encore disponible.</p>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$current_mode = 'final'">
                <xsl:choose>
                    <xsl:when
                        test="count(/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]) > 0">
                        <div class="transcription final  m-4 col-11">
                            <xsl:apply-templates mode="final"
                                select="/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]/tei:text/tei:body"
                            />
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <p class="info">Cette fiche n'est pas encore disponible.</p>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$current_mode = 'img'">
                <xsl:value-of select="concat($pagenum, '.jpg')"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'is_xml_available'">
                <xsl:choose>
                    <xsl:when
                        test="count(/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]) > 0">
                        <xsl:text>1</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>0</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$current_mode = 'title'">
                <xsl:choose>
                    <xsl:when
                        test="count(/tei:teiCorpus/tei:TEI[.//tei:surface/@xml:id = concat($pagenum, '.jpg')]) > 0">
                        <xsl:value-of select="$pagenum"/>
                        <xsl:text> (transcrit)</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-before(.//tei:surface/@xml:id, '.jpg')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$current_mode = 'list_available_xml'">
                <xsl:for-each select="/tei:teiCorpus/tei:TEI">
                    <xsl:choose>
                        <xsl:when
                            test="string-length(substring-before(.//tei:surface/@xml:id, '.jpg')) = 0">
                            <xsl:comment>Attention: @xml:id erroné (devrait être de la forme xxx.jpg) pour "<xsl:value-of select=".//tei:surface/@xml:id"/>"</xsl:comment>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring-before(.//tei:surface/@xml:id, '.jpg')"/>
                            <xsl:text>
</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="$current_mode = 'count_available_xml'">
                <xsl:choose>
                    <xsl:when test="$boxnum = 'all'">
                        <xsl:value-of select="count(/tei:teiCorpus/tei:TEI)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                            select="count(/tei:teiCorpus/tei:TEI//tei:surface[substring(@xml:id, 5, 2) = $boxnum])"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$current_mode = 'list_all_leaflet'">
                <xsl:for-each select="/tei:teiCorpus/*">
                    <!--<xsl:value-of select="name(.)"/>-->
                    <xsl:choose>
                        <xsl:when test="name() = 'teiHeader'"/>
                        <xsl:when test="name() = 'text'">
                            <xsl:value-of select="substring-before(@corresp, '.jpg')"/>
                        </xsl:when>
                        <xsl:when test="name() = 'TEI'">
                            <xsl:value-of select="substring-before(.//tei:surface/@xml:id, '.jpg')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="name()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:text>
</xsl:text>
                </xsl:for-each>
                <xsl:for-each select="//xi:include">
                    <xsl:value-of select="@href"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <p class="info">Cette information n'est pas disponible.</p>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="
            $standalone = 1
            and $ask != 'list_available_xml'
            and $ask != 'list_all_leaflet'
            and $ask != 'count_available_xml'
            and $ask != 'is_xml_available'
            and $ask != 'title'
            ">
            <xsl:text disable-output-escaping="yes">
    &lt;/body>
&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
