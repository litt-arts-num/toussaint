
document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);

function init() {



	ScreenSize();
  registerListener(window,'resize', ScreenSize);

  //registerListener(window,'scroll', listenScroll);
  var main = document.querySelector("main");
  main.addEventListener('scroll', listenScroll);


  var el = document.querySelector('.cache');
  var options = {
    onMouseMove: placeButtons,
  };
  displacejs(el, options);

  setTimeout(function(){
    calculNav();
    caleTranscriptionAvecManuscrit();
    placeButtons(el);
  }, 100);

	var csv = document.getElementById('data-url').getAttribute('href');
  parseData(csv, MakerOfObject);




  var els = document.querySelectorAll('.buttons div');
  for (var i = 0; i < els.length; i++) {
    var options2 = {
      onMouseMove: updateClip,
    };
    displacejs(els[i], options2);
  }



  var zoom_buttons = document.querySelectorAll(".zoom-buttons button");
  for (var i = 0; i < zoom_buttons.length; i++) {
    zoom_buttons[i].addEventListener("click", zoom);
  }

}


function calculNav() {
  var navbar = document.querySelector("#navbar");
  var main = document.querySelector("main");
  var container_transcription = document.querySelector("#container_transcription");
  var manuscrit_img = document.querySelector("#manuscrit_img");
  var transcription = document.querySelector("#transcription");
  var offsetHeight = navbar.clientHeight;
  console.log(offsetHeight);

  main.style.height = "calc(100vh - "+offsetHeight+"px)";

  var img = manuscrit_img.querySelector("img");
  var cache = document.querySelector(".cache");

  img.style.height = "calc(100vh - "+offsetHeight+"px)";
  cache.style.height = "calc(100vh - "+offsetHeight+"px)";
  transcription.style.height = "calc(100vh - "+offsetHeight+"px)";

  container_transcription.style.height = "calc(100vh - "+offsetHeight+"px)";
  container_transcription.style.top = offsetHeight+"px";

  container_transcription.style.width = manuscrit_img.clientWidth+"px";

}

function listenScroll() {
  var scroll_value = this.scrollTop;
  // console.log(scroll_value);
  if (typeof current_scale !== 'undefined') {

  } else {
    current_scale = 1;
  }

  var transcription = document.querySelector("#transcription");
  var translate =  ((scroll_value) / screen_height * 100) * -1;
  transcription.style.transform = "translate(0, "+ ((scroll_value * -1) / current_scale) +"px)";
}

function registerListener(el, event, func) {
    if (el.addEventListener) {
        el.addEventListener(event, func)
    } else {
        el.attachEvent('on' + event, func)
    }
}

function zoom() {
  var manuscrit_img = document.querySelector("#manuscrit_img");
  var container_transcription = document.querySelector("#container_transcription");

  var css_text = manuscrit_img.style.cssText;
  current_scale = parseFloat(css_text.match(new RegExp(/(scale\(([^)]+)\))/))[0].match(new RegExp(/\(([^)]+)\)/))[1]);
  // console.log(current_scale);


  if (this.classList.contains("zoom-in")) {
    current_scale += .2;
    manuscrit_img.style.transform = "scale("+ current_scale +")";
    container_transcription.style.transform = "scale("+ current_scale +")";
    console.log(current_scale);
  } else {
    if (current_scale > 1) {
      current_scale -= .2;
      manuscrit_img.style.transform = "scale("+ current_scale +")";
      container_transcription.style.transform = "scale("+ current_scale +")";
    }
  }

}




function updateClip(el) {
  var cache = document.querySelector(".cache");
  var buttons = document.querySelectorAll(".buttons div");

  var clip_css = getComputedStyle(cache).clipPath;
  if (clip_css == "none") var clip_css = getComputedStyle(cache).webkitClipPath;
  var regex = /\d+/g;
  var coords = clip_css.match(regex).map(Number);

  // debug si shorthand du clip-path
  switch (coords.length) {
    case 1:
      for (var i = 0; i < 3; i++) coords.push(coords[0]);
      break;
    case 2:
      coords.push(coords[0]);
      coords.push(coords[1]);
      break;
    case 3:
      coords.push(coords[1]);

      break;
  }

  var top = coords[0];
  var right = coords[1];
  var bottom = coords[2];
  var left = coords[3];

  var width = 100 - (coords[1] + coords[3]);
  var height = 100 - (coords[2] + coords[0]);

  if (el.classList.contains("b_top")) {
  if (cache.offsetTop < el.offsetTop && cache.offsetLeft < el.offsetLeft) {
    var decallage_top = el.offsetTop - cache.offsetTop + 20;
    var decallage_left = el.offsetLeft - cache.offsetLeft + 10;

    var new_top = Math.round(decallage_top / screen_height * 100) + "%";
    var new_left = Math.round(decallage_left / screen_width * 100) + "%";

    cache.style.clipPath = "inset("+new_top+" "+right+"% "+bottom+"% "+new_left+")";
    cache.style.webkitClipPath = "inset("+new_top+" "+right+"% "+bottom+"% "+new_left+")";
  }
  } else {
    var decallage_bottom = (screen_height - el.offsetTop) + cache.offsetTop - 40;
    var decallage_right = (screen_width - el.offsetLeft) + cache.offsetLeft - 34;

    var new_bottom = Math.round(decallage_bottom / screen_height * 100) + "%";
    var new_right = Math.round(decallage_right / screen_width * 100) + "%";
    // sconsole.log(el.offsetTop);
    // sconsole.log(decallage_right);

    cache.style.clipPath = "inset("+top+"% "+new_right+" "+new_bottom+" "+left+"%)";
    cache.style.webkitClipPath = "inset("+top+"% "+new_right+" "+new_bottom+" "+left+"%)";
  }
}

function placeButtons(el) {

  var cache = document.querySelector(".cache");

  var buttons = document.querySelectorAll(".buttons div");

  var clip_css = getComputedStyle(cache).clipPath;
  if (clip_css == "none") {
    clip_css = "40% 40% 50% 40%";
    console.log(getComputedStyle(cache).clipPath);
    console.log(clip_css);
  }

  var regex = /\d+/g;
  // if (clip_css == "none") var clip_css = getComputedStyle(cache).webkitClipPath;
  var coords = clip_css.match(regex).map(Number);

  // debug si shorthand du clip-path
  switch (coords.length) {
    case 1:
      for (var i = 0; i < 3; i++) coords.push(coords[0]);
      break;
    case 2:
      coords.push(coords[0]);
      coords.push(coords[1]);
      break;
    case 3:
      coords.push(coords[1]);
      break;
  }

  var top = coords[0];
  var left = coords[3];
  var top_px = (top * screen_height) / 100;
  // console.log(el.offsetTop);
  var left_px = (left * screen_width) / 100;
  var width = 100 - (coords[1] + coords[3]);
  var height = 100 - (coords[2] + coords[0]);

  for (var i = 0; i < buttons.length; i++) {
    switch (i) {
    case 0:
      // buttons[i].style.top = top + "%";
      // buttons[i].style.left = (left + width/2) + "%";
      buttons[i].style.top = top_px + (el.offsetTop - 22) + "px";
      buttons[i].style.left = left_px + (el.offsetLeft - 8) + "px";
      break;
    case 1:
      // buttons[i].style.top = (top + height) + "%";
      // buttons[i].style.left = (left + width/2) + "%";
      buttons[i].style.top = "calc(" + (top + height) + "% + " + (el.offsetTop - 12) + "px)";
      buttons[i].style.left = "calc(" + (left + width) + "% + " + (el.offsetLeft - 13) + "px)";
      break;
    }
  }

}




function caleTranscriptionAvecManuscrit() {
  var img_manuscrit = document.querySelector("#manuscrit_img figure img");
  var div_transcription = document.querySelector("#transcription");

  imagesLoaded(img_manuscrit, function( instance ) {
    var width = img_manuscrit.clientWidth;
    console.log(img_manuscrit.clientWidth);
    div_transcription.style.width = width + "px";
  });

  div_transcription.style.fontSize = (Math.round( (screen_height / 92.5) * 100) / 100) + "px";

}

function MakerOfObject(data) {
  for (var i = 0; i < data.length - 1; i++) {
    var object = new Object();
    object.contenu = data[i]['contenu'];
    object.intervention = data[i]['intervention'];
    object.rangIntervention = data[i]['rangIntervention'];
    object.style = data[i]['style'];
    object.nature = data[i]['nature'];

    object.class = data[i]['class'];
    object.rang = data[i]['rang'];

    object.width = data[i]['width'];
    object.height = data[i]['height'];
    object.left = data[i]['left'];
    object.top = data[i]['top'];
    object.rotate = data[i]['rotate'];

    object.width = data[i]['left2'] - data[i]['left'];
    // object.height = data[i]['top2'] - data[i]['top'];
    object.height = 1.1;


    object.leftDiplomatique = data[i]['leftDiplomatique'];
    object.topDiplomatique = data[i]['topDiplomatique'];

    // displayZone(object);
    displayObject(object);
  }
}

function displayZone(obj) {
  var zoom = 1;
  var div_transcription = document.querySelector("#transcription");

  var new_span = document.createElement("SPAN");
  new_span.style.left = zoom * obj.left + "%";
  new_span.style.top = zoom * obj.top + "%";
  new_span.style.width = zoom * obj.width + "%";
  new_span.style.height = zoom * obj.height + "%";
  if (obj.rotate) new_span.style.transform = "rotate(" + obj.rotate + "deg)";

  div_transcription.appendChild(new_span);
}






function displayObject(obj) {
  var zoom = 1;
  var div_transcription = document.querySelector("#transcription");

  var new_span = document.createElement("SPAN");
  new_span.innerHTML = obj.contenu;
  new_span.style.left = zoom * obj.left + "%";
  if (obj.nature == "manuscrit") {
    new_span.className += " " + obj.nature;
    new_span.style.fontSize = (Math.round( (screen_height / 110) * 100) / 100) + "px";
  } else {

  }


  new_span.className += " " + obj.style;
  new_span.style.top = zoom * (obj.top - .55) + "%";
  if (obj.rotate) new_span.style.transform = "rotate(" + obj.rotate + "deg)";

  /*
  var selectorMANUSCRIT = document.querySelector(data.selectorMANUSCRIT);
    var selectorDIPLOMATIQUE = document.querySelector(data.selectorDIPLOMATIQUE);

    selectorMANUSCRIT.style.width = zoom * data.width + "%";
    selectorMANUSCRIT.style.height = zoom * data.height + "%";

    $(data.selectorMANUSCRIT).css("left", zoom * data.left + "%");
    $(data.selectorMANUSCRIT).css("top", zoom * data.top + "%");

    $(data.selectorMANUSCRIT).css("-ms-transform", "rotate(" + data.rotate + "deg)");
    $(data.selectorMANUSCRIT).css("-webkit-transform", "rotate(" + data.rotate + "deg)");
    $(data.selectorMANUSCRIT).css("transform", "rotate(" + data.rotate + "deg)");

    $("#debug").css("width", zoom * 100 + "%");
    $("#debug").css("height", zoom * 100 + "%");

  */


  div_transcription.appendChild(new_span);
}



function parseData(url, callBack) {
    Papa.parse(url, {
        download: true,
        header: true,
        complete: function(results) {
            callBack(results.data);
        }
    });
}



function ScreenSize(){
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];

    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
    screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}

var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};




/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];




****** DÉCLARATION DE FONCTION ******

function maFonction() {
	console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
	console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
	console.log("vrai");
}

if (condition1) {
	console.log("condition1 : vrai");
} else if (condition2) {
	console.log("condition2 : vrai");
} else {
	console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut", "valeur_attribut");
el.getAttribute("nom_attribut");




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended", "maFonction()");
video.setAttribute("controls", "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
	console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/
