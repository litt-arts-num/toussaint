
document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);

function init() {

	ScreenSize();
  // registerListener(window,'resize', ScreenSize);

  caleTranscriptionAvecManuscrit();

  var csv = "data/csv/LRT_01_01_00003.csv";
  parseData(csv, MakerOfObject);

  // zoom
  var zoom_buttons = document.querySelectorAll(".zoom-buttons button");
  for (var i = 0; i < zoom_buttons.length; i++) {
    zoom_buttons[i].addEventListener("click", zoom);
  }
}




function zoom() {
  var container = document.querySelector("#container_manuscrit");
  
  var css_text = container.style.cssText;
  current_scale = parseFloat(css_text.match(new RegExp(/(scale\(([^)]+)\))/))[0].match(new RegExp(/\(([^)]+)\)/))[1]);  
  
    if (this.classList.contains("zoom-in")) {
      current_scale += .2;
      
      
      container.style.transform = "scale("+ current_scale +")";
      
    } else {
      if (current_scale > 1) {
        current_scale -= .2;
        
        container.style.transform = "scale("+ current_scale +")";
      }
    }
}

function caleTranscriptionAvecManuscrit() {
  var img_manuscrit = document.querySelector("#container_img img");
  var div_transcription = document.querySelector("#container_transcription");

  img_manuscrit.addEventListener("load", function() {
    var width = img_manuscrit.clientWidth;
    var height = img_manuscrit.clientHeight;
    div_transcription.style.width = width + "px";
    div_transcription.style.height = height + "px";
  });

}

function MakerOfObject(data) {
  var objectsArray = [];

  /*
  var stop = 0;
  for (var i = 0; i < data.length - 1; i++) {
    if (data[i]['class'] === "") {
      stop = i;
      break;
    }
  }
  */

  for (var i = 0; i < data.length - 1; i++) {
  // for (var i = 0; i < stop; i++) {
    var object = new Object();
    object.contenu = data[i]['contenu'];
    object.intervention = data[i]['intervention'];
    object.rangIntervention = data[i]['rangIntervention'];
    object.nature = data[i]['nature'];

    object.class = data[i]['class'];
    object.rang = data[i]['rang'];

    object.left = data[i]['left'];
    object.top = data[i]['top'];
    object.rotate = data[i]['rotate'];

    object.width = data[i]['left2'] - data[i]['left'];
    // object.height = data[i]['top2'] - data[i]['top'];
    object.height = 1.1;

    object.leftDiplomatique = data[i]['leftDiplomatique'];
    object.topDiplomatique = data[i]['topDiplomatique'];

    objectsArray.push(object);
    // big_object.push({object: object});
    // displayObject(object);
    
  }

  displayZoneTranscription(objectsArray);
  displayObjectInArticles(objectsArray);
}


function displayZoneTranscription(obj) {
  
  // new_span.style.width = zoom * obj.width + "%";
  // new_span.style.height = zoom * obj.height + "%";

  var zoom = 1;
  var container_transcription = document.querySelector("#container_transcription");
  for (var i = 0; i <= obj.length - 1; i++) {
    var new_span = document.createElement("SPAN");

    new_span.className = "step" + obj[i].class;
    new_span.setAttribute("data-step", obj[i].class);

    new_span.style.top = obj[i].top + "%";
    new_span.style.left = obj[i].left + "%";
    new_span.style.width = obj[i].width + "%";
    // new_span.style.height = zoom * obj.height + "%";
    new_span.style.height = zoom * obj[i].height + "%";

    new_span.style.transform = "rotate(" + obj[i].rotate + "deg)"; 
    new_span.style.webkitTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.MozTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.msTransform = "rotate(" + obj[i].rotate + "deg)";

    new_span.addEventListener("mouseover", hoverSpanTranscription);
    new_span.addEventListener("mouseleave", hoverSpanTranscription);
    new_span.addEventListener("click", scrollToArticle);

    container_transcription.appendChild(new_span);
  }

}

function scrollToArticle() {
  var currentClass = this.getAttribute("data-step");
  var article = document.querySelectorAll("#container article")[currentClass];

  var valeurScroll = (parseInt(currentClass) + 1) * screen_height;
  window.scrollTo({ top: valeurScroll, left: 0, behavior: 'smooth' });
}

function hoverSpanTranscription(event) {

  var currentClass = this.getAttribute("data-step");
  var otherEls = document.querySelectorAll('#container_transcription span[data-step="' + currentClass + '"]');
  var alreadyHovered = document.querySelectorAll(".hovered");

  for (var i = 0; i < alreadyHovered.length; i++) {
    alreadyHovered[i].classList.remove("hovered");
  }
  for (var i = 0; i < otherEls.length; i++) {
    otherEls[i].classList.add("hovered");
  }
  
}
  
function displayObjectInArticles(obj) {
  var classTotal = obj[obj.length - 1].class;
  
  for (var i = 0; i <= classTotal; i++) {
    var results = obj.filter(function (entry) { return entry.class === i.toString(); });
    var temp_array = [];
    for (var j = 0; j < results.length; j++) {
      temp_array.push(results[j].contenu);
    }
    var string = temp_array.join(" ");

    var container = document.querySelector("#container");
    var article = document.createElement("ARTICLE");
    var p = document.createElement("P");
    var span = document.createElement("SPAN");
    span.className = "visible";
    span.innerHTML = " " + string;
    p.appendChild(span);
    article.appendChild(p);
    container.appendChild(article);

    var current_articles = document.querySelectorAll("#container article p");

    // ajout
    var object_init = results[0].intervention;

    if (object_init == "ajout") {
      var previous_text = current_articles[i - 1].textContent;

      var span_invisible = document.createElement("SPAN");
      span_invisible.className = "invisible";
      span_invisible.innerHTML = previous_text;
        
      p.insertBefore(span_invisible, span);
    }

    if (object_init == "remplacement") {
      
      var rang = results[0].rangIntervention;
      var article_src = current_articles[rang - 1];
      var article_content = article_src.textContent;

      var span_invisible = document.createElement("SPAN");
      span_invisible.className = "invisible";
      span_invisible.innerHTML = article_content;

      p.insertBefore(span_invisible, span);


      var article_précédent = current_articles[i - 1].innerHTML;
      var b = '<b class="barre">';
      var length = article_src.textContent.length + 24;
      
      var essai = article_précédent.insert((length), b) + '</b>';

      current_articles[i - 1].innerHTML = essai;

    }

  }

  // scroll_article.js
  loop();
}



String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};


function displayObject(obj) {
  var zoom = 1;
  var div_transcription = document.querySelector("#transcription");

  var new_span = document.createElement("SPAN");
  new_span.innerHTML = obj.contenu;
  new_span.style.left = zoom * obj.leftDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";

  new_span.style.top = zoom * obj.topDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";

  div_transcription.appendChild(new_span);
}

function parseData(url, callBack) {
    Papa.parse(url, {
        download: true,
        header: true,
        complete: function(results) {
            callBack(results.data);
        }
    });
}



function ScreenSize(){
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];
    
    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
    screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}

var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};







// https://gist.github.com/Warry/4254579
// Detect request animation frame
var scroll = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame ||
             // IE Fallback, you can even fallback to onscroll
             function(callback){ window.setTimeout(callback, 1000/60) };

var lastPosition = -1;

function loop(){
    // Avoid calculations if not needed
    if (lastPosition == window.pageYOffset) {
        scroll(loop);
        return false;
    } else lastPosition = window.pageYOffset;


    var NbArticles = document.querySelectorAll("#container article").length;
    for (var j = 0; j <= NbArticles; j++) {
        if (lastPosition >  (j * (screen_height) + screen_height)) {
            // console.log(j);
            fixe(j);
            disparition(j);
        } else {
            réapparition(j);
        }

        if (lastPosition >  (j * (screen_height))) {
            highlightTranscription(j);
        }
    }



    scroll(loop);
}


var couleurNormal = '#282083';
var couleurActive = '#ff0000';

function highlightTranscription(e) {
    // span sur manuscrit
    var vieux_spans = "";
    var spans = "";

    var spans = document.querySelectorAll('#container_transcription span[data-step="' + e + '"]');
    var alreadyHovered = document.querySelectorAll('#container_transcription .hovered');
    for (var i = 0; i < alreadyHovered.length; i++) {
        alreadyHovered[i].classList.remove("hovered");
    }
    for (var i = 0; i < spans.length; i++) {
        spans[i].classList.add("hovered");
    }       
}

function fixe(e) {
    var articles = document.querySelectorAll("#container article");
    // les articles
    if (articles[e - 1]) articles[e - 1].style.opacity = 0;
    articles[e].querySelector("span").style.opacity = 1;
    
}

// Fonction pour faire disparaître - apparître progressivement
// des mots lors d'un remplacement
var opacité = 0;
function disparition(e) {
    var articles = document.querySelectorAll("#container article");
    var opacité = p5.map(lastPosition, ((e * screen_height) + screen_height) , ((screen_height * (e + 1)) + screen_height), 1, 0);
    if (articles[e].querySelector(".barre")) {
        for (var i = 0; i < articles[e].querySelectorAll(".barre").length; i++) {
            articles[e].querySelectorAll(".barre")[i].style.opacity = opacité;
        }
    }
}


// Fonction pour permettre la réapparition de l'article précédent
function réapparition(e) {
    // les articles
    var articles = document.querySelectorAll("#container article");
    if (articles[e - 1]) articles[e - 1].style.opacity = 1;
    if (articles[e] != undefined && articles[e].querySelector("span:not(.visible)")) {
        articles[e].querySelector("span:not(.visible)").style.opacity = 0;
    }
    
}




/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];




****** DÉCLARATION DE FONCTION ******

function maFonction() {
	console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
	console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
	console.log("vrai");
}

if (condition1) {
	console.log("condition1 : vrai");
} else if (condition2) {
	console.log("condition2 : vrai");
} else {
	console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut", "valeur_attribut");
el.getAttribute("nom_attribut");  




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended", "maFonction()");
video.setAttribute("controls", "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
	console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/




