// https://gist.github.com/Warry/4254579
// Detect request animation frame
var scroll = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame ||
             // IE Fallback, you can even fallback to onscroll
             function(callback){ window.setTimeout(callback, 1000/60) };

var lastPosition = -1;

function loop(){
    // Avoid calculations if not needed
    if (lastPosition == window.pageYOffset) {
        scroll(loop);
        return false;
    } else lastPosition = window.pageYOffset;


    var NbArticles = document.querySelectorAll("#container article").length;
    for (var j = 0; j <= NbArticles; j++) {
        if (lastPosition >  (j * (screen_height) + screen_height)) {
            // console.log(j);
            fixe(j);
            disparition(j);
        } else {
            réapparition(j);
        }

        if (lastPosition >  (j * (screen_height))) {
            highlightTranscription(j);
        }
    }



    scroll(loop);
}


var couleurNormal = '#282083';
var couleurActive = '#ff0000';

function highlightTranscription(e) {
    // span sur manuscrit
    var vieux_spans = "";
    var spans = "";

    var spans = document.querySelectorAll('#container_transcription span[data-step="' + e + '"]');
    var alreadyHovered = document.querySelectorAll('#container_transcription .hovered');
    for (var i = 0; i < alreadyHovered.length; i++) {
        alreadyHovered[i].classList.remove("hovered");
    }
    for (var i = 0; i < spans.length; i++) {
        spans[i].classList.add("hovered");
    }       
}

function fixe(e) {
    var articles = document.querySelectorAll("#container article");
    // les articles
    if (articles[e - 1]) articles[e - 1].style.opacity = 0;
    articles[e].querySelector("span").style.opacity = 1;
    
}

// Fonction pour faire disparaître - apparître progressivement
// des mots lors d'un remplacement
var opacité = 0;
function disparition(e) {
    var articles = document.querySelectorAll("#container article");
    var opacité = p5.map(lastPosition, ((e * screen_height) + screen_height) , ((screen_height * (e + 1)) + screen_height), 1, 0);
    if (articles[e].querySelector(".barre")) {
        for (var i = 0; i < articles[e].querySelectorAll(".barre").length; i++) {
            articles[e].querySelectorAll(".barre")[i].style.opacity = opacité;
        }
    }
}


// Fonction pour permettre la réapparition de l'article précédent
function réapparition(e) {
    // les articles
    var articles = document.querySelectorAll("#container article");
    if (articles[e - 1]) articles[e - 1].style.opacity = 1;
    if (articles[e] != undefined && articles[e].querySelector("span:not(.visible)")) {
        articles[e].querySelector("span:not(.visible)").style.opacity = 0;
    }
    
}






/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];




****** DÉCLARATION DE FONCTION ******

function maFonction() {
    console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
    console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
    console.log("vrai");
}

if (condition1) {
    console.log("condition1 : vrai");
} else if (condition2) {
    console.log("condition2 : vrai");
} else {
    console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut", "valeur_attribut");
el.getAttribute("nom_attribut");  




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA screen_height ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended", "maFonction()");
video.setAttribute("controls", "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
    console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/




