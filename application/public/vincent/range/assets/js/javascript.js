
document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);

function init() {

	ScreenSize();
  // registerListener(window,'resize', ScreenSize);
  window.addEventListener('resize', function() {
    ScreenSize();
    resizeElements();
  }, true);

  // calculNav();

  caleTranscriptionAvecManuscrit();

	var csv = document.getElementById('data-url').getAttribute('href');
  parseData(csv, MakerOfObject);

  // zoom
  var zoom_buttons = document.querySelectorAll(".zoom-buttons button");
  for (var i = 0; i < zoom_buttons.length; i++) {
    zoom_buttons[i].addEventListener("click", zoom);
  }
}



function resizeElements() {
}


function calculNav() {
  var navbar = document.querySelector("#navbar");
  var main = document.querySelector("main");
  main.style.height = "calc(70vh - "+navbar.offsetHeight+"px)";
}



function calcClip(els) {
  var tableau_top = [];
  var tableau_right = [];
  var tableau_bottom = [];
  var tableau_left = [];

  for (var i = 0; i < els.length; i++) {
    var top = parseInt(getComputedStyle(els[i]).top);
    var right = parseInt(getComputedStyle(els[i]).right);
    var bottom = parseInt(getComputedStyle(els[i]).bottom);
    var left = parseInt(getComputedStyle(els[i]).left);

    tableau_top.push(top);
    tableau_right.push(right);
    tableau_bottom.push(bottom);
    tableau_left.push(left);
  }

  var min_top = Array.min(tableau_top) - 5;
  var min_right = Array.min(tableau_right) - 10;
  var min_bottom = Array.min(tableau_bottom) - 5;
  var min_left = Array.min(tableau_left) - 10;


  var facsimile = document.querySelector("#facsimile img");
  var width = facsimile.clientWidth;
  var height = facsimile.clientHeight;

  var p_top = min_top / height * 100 + "%";
  var p_right = min_right / width * 100 + "%";
  var p_bottom = min_bottom / height * 100 + "%";
  var p_left = min_left / width * 100 + "%";


  var cache = document.querySelector("#cache");
  cache.style.clipPath = "inset("+p_top+" "+p_right+" "+p_bottom+" "+p_left+")";


  var container = document.querySelector(".container");
  var css_text = container.style.cssText;
  current_scale = parseFloat(css_text.match(new RegExp(/(scale\(([^)]+)\))/))[0].match(new RegExp(/\(([^)]+)\)/))[1]);

  var panneaux = document.querySelectorAll(".panneau");
  var hauteur_panneau = panneaux[0].offsetHeight;
  var largeur_panneau = panneaux[0].offsetWidth;
  var max_left = width - min_right;
  var max_width = ((max_left - min_left) /2) + min_left;

  var main = document.querySelector("main");


  var scrollLeft = (((max_left - min_left) /2) + min_left + (40)) * current_scale - (largeur_panneau / 2);
  var scrollTop = (min_top + 25) * current_scale - (hauteur_panneau / (2));

  for (var i = 0; i < panneaux.length; i++) {
    panneaux[i].scrollTo({ top: scrollTop, left: scrollLeft, behavior: 'smooth' });
  }
}

Array.max = function( array ){
    return Math.max.apply( Math, array );
};
Array.min = function( array ){
    return Math.min.apply( Math, array );
};



function zoom() {
  var containers = document.querySelectorAll(".container");
  var container_tr = document.querySelector("#container_transcription");

  var css_text = containers[0].style.cssText;
  current_scale = parseFloat(css_text.match(new RegExp(/(scale\(([^)]+)\))/))[0].match(new RegExp(/\(([^)]+)\)/))[1]);



    if (this.classList.contains("zoom-in")) {
      current_scale += .2;

      // container_tr.style.transform = "scale("+ current_scale +")";
      for (var i = 0; i < containers.length; i++) {
        containers[i].style.transform = "scale("+ current_scale +")";
      }
    } else {
      if (current_scale > 1) {
        current_scale -= .2;

        // container_tr.style.transform = "scale("+ current_scale +")";
        for (var i = 0; i < containers.length; i++) {
          containers[i].style.transform = "scale("+ current_scale +")";
        }
      }
    }
}




function caleTranscriptionAvecManuscrit() {
  var facsimile = document.querySelector("#facsimile img");
  var tr_natures = document.querySelector("#transcription_natures");
  var cont_tr = document.querySelector("#container_transcription");

  imagesLoaded(facsimile, function( instance ) {
    console.log('all images are loaded');
    var width = facsimile.clientWidth;
    var height = facsimile.clientHeight;

    tr_natures.style.width = width + "px";
    tr_natures.style.height = height + "px";

    cont_tr.style.width = width + "px";
    cont_tr.style.height = height + "px";

  });

  facsimile.addEventListener("load", function() {

    var font = new FontFaceObserver('Spacemono');
    font.load().then(function () {
      // load
    }, function () {
        console.log('Font is not available');
    });

  });

}

function MakerOfObject(data) {
  objectsArray = [];

  /*
  var stop = 0;
  for (var i = 0; i < data.length - 1; i++) {
    if (data[i]['class'] === "") {
      stop = i;
      break;
    }
  }
  */

  for (var i = 0; i < data.length - 1; i++) {
  // for (var i = 0; i < stop; i++) {
    var object = new Object();
    object.contenu = data[i]['contenu'];
    object.intervention = data[i]['intervention'];
    object.rangIntervention = data[i]['rangIntervention'];
    object.nature = data[i]['nature'];
    object.style = data[i]['style'];

    object.class = data[i]['class'];
    object.rang = data[i]['rang'];

    object.left = data[i]['left'];
    object.top = data[i]['top'];
    object.rotate = data[i]['rotate'];

    object.width = data[i]['left2'] - data[i]['left'];
    // object.height = data[i]['top2'] - data[i]['top'];
    // object.height = 1.1;

    object.leftDiplomatique = data[i]['leftDiplomatique'];
    object.topDiplomatique = data[i]['topDiplomatique'];

    objectsArray.push(object);
    // big_object.push({object: object});
    // displayObject(object);

  }

  displayZoneTranscription(objectsArray);
  displayNatures(objectsArray);
  displayRange(objectsArray);
}



function displayZoneTranscription(obj) {

  // new_span.style.width = zoom * obj.width + "%";
  // new_span.style.height = zoom * obj.height + "%";

  var zoom = 1;
  var container_transcription = document.querySelector("#container_transcription");
  for (var i = 0; i <= obj.length - 1; i++) {
    var new_span = document.createElement("SPAN");

    new_span.className = "step" + obj[i].class;
    new_span.setAttribute("data-step", obj[i].class);

    new_span.style.top = obj[i].top + "%";
    new_span.style.left = obj[i].left + "%";
    new_span.style.width = obj[i].width + "%";
    // new_span.style.height = zoom * obj.height + "%";
    new_span.style.height = "1.1%";

    new_span.style.transform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.webkitTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.MozTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.msTransform = "rotate(" + obj[i].rotate + "deg)";

    new_span.addEventListener("mouseover", hoverSpanTranscription);
    new_span.addEventListener("mouseleave", hoverSpanTranscription);

    container_transcription.appendChild(new_span);
  }

}


function displayRange(obj) {
  var steps = parseInt(obj[obj.length - 1].class);
  var input = createRange(0, steps, true);
  var aside = document.querySelector("aside");
  aside.appendChild(input);

  var p = document.createElement("P");
  p.innerHTML = "séquences d’écriture";
  aside.appendChild(p);

  for (var i = 0; i <= obj.length - 2; i++) {
    var startStep = obj[i].nature == "tapuscrit" && obj[i + 1].nature == "manuscrit";

    if (startStep) {
      var min = parseInt(obj[i].class);
      for (var j = i; j < obj.length - 1; j++) {
        var endStep = obj[j].nature == "manuscrit" && obj[j + 1].nature == "tapuscrit";
        if (endStep) {
          var max = parseInt(obj[j].class);
          break;
        }
      }

      var steps_number = max - min;
      var width = steps_number / steps * 100;

      var input = createRange(min, (max - 1));
      // console.log(min);
      input.style.width = steps_number / steps * 100 + "%";

      // input.style.marginLeft = ((min+1) * steps / 150) + (min-0) + "%";
      console.log(steps_number / steps);
      // mouseValue * range.max / screen.max
      // snum.map(min, steps, 0, 100);
      input.style.marginLeft = min.map(0, (steps +0), 0, 100) + "%";
      // sinput.style.marginLeft = min + "px";

      aside.appendChild(input);
    }
  }
}

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


function createRange(min, max, master) {
  var input = document.createElement("INPUT");
  input.type = "range";
  input.id = "";

  input.min = min;
  input.max = parseInt(max) + 1;
  input.step = 1;
  input.value= 0;

  if (master) {
    input.addEventListener("input", inputMasterChanging);
  } else {
    input.addEventListener("input", inputChanging);
  }

  return input;
}


function inputChanging() {
  var val = parseInt(this.value);
  var min = parseInt(this.min);
  var max = parseInt(this.max);


  var spans = document.querySelectorAll("#transcription_natures span");

  // steps precedents
  for (var i = min; i <= val; i++) {
    // console.log(val);

    // var test = document.querySelector('#transcription_natures span[data-step="' + i + '"]');
    // if (!test.classList.contains("current_step")) {

    var els = document.querySelectorAll('#transcription_natures span[data-step="' + i + '"]');
    var current_els = document.querySelectorAll('#transcription_natures span[data-step="' + val + '"]');
    var current_els_tr = document.querySelectorAll('#container_transcription span[data-step="' + val + '"]');
    var already_current = document.querySelectorAll(".current_step");
    var already_hovered = document.querySelectorAll(".hovered");

    for (var j = 0; j < already_current.length; j++) {
      already_current[j].classList.remove("current_step");
      already_hovered[j].classList.remove("hovered");
    }
    for (var j = 0; j < current_els.length; j++) {
      current_els[j].classList.add("current_step");
      current_els_tr[j].classList.add("hovered");
    }

    if (i > 1) {
      var prec_els = document.querySelectorAll('#transcription_natures span[data-step="' + (i - 1) + '"]');
    } else {
      var prec_els = [];
    }

    for (var j = 0; j < els.length; j++) {

      if (els[0].classList.contains("manuscrit")) {
        els[j].classList.add("visible");
      } else if (els[0].classList.contains("tapuscrit")) {
        els[j].classList.add("activated");
      }
      if (prec_els.length) {
        if (prec_els[0].classList.contains("barre")) {
          for (var k = 0; k < prec_els.length; k++) {
            prec_els[k].classList.add("activated");
          }
        }
      }

    }

  }

  // steps suivants
  for (var i = val + 1; i <= max; i++) {

    /// / var test = document.querySelector('#transcription_natures span[data-step="' + i + '"]');
    // if (!test.classList.contains("current_step")) {

    var els = document.querySelectorAll('#transcription_natures span[data-step="' + i + '"]');
    for (var j = 0; j < els.length; j++) {
      els[j].classList.remove("current_step");
    }

    for (var j = 0; j < els.length; j++) {

      if (els[0].classList.contains("manuscrit")) {
        els[j].classList.remove("visible");
      } else if (els[0].classList.contains("tapuscrit")) {
        els[j].classList.remove("activated");
      }

    }


  }

}
function inputMasterChanging() {
  // console.log("inputMasterChanging");
  var val = parseInt(this.value);
  var max = parseInt(this.max);
  // console.log("value " + this.value);


  var spans = document.querySelectorAll("#transcription_natures span");

  // steps precedents

  for (var i = 0; i <= val; i++) {
    // console.log(val);

    // var test = document.querySelector('#transcription_natures span[data-step="' + i + '"]');
    // if (!test.classList.contains("current_step")) {

    var els = document.querySelectorAll('#transcription_natures span[data-step="' + i + '"]');
    var current_els = document.querySelectorAll('#transcription_natures span[data-step="' + val + '"]');
    var current_els_tr = document.querySelectorAll('#container_transcription span[data-step="' + val + '"]');
    var already_current = document.querySelectorAll(".current_step");
    var already_hovered = document.querySelectorAll(".hovered");

    for (var j = 0; j < already_current.length; j++) {
      already_current[j].classList.remove("current_step");
      already_hovered[j].classList.remove("hovered");
    }
    for (var j = 0; j < current_els.length; j++) {
      current_els[j].classList.add("current_step");
      current_els_tr[j].classList.add("hovered");
    }

    if (i > 1) {
      var prec_els = document.querySelectorAll('#transcription_natures span[data-step="' + (i - 1) + '"]');
    } else {
      var prec_els = [];
    }

    for (var j = 0; j < els.length; j++) {

      if (els[0].classList.contains("manuscrit")) {
        els[j].classList.add("visible");
      } else if (els[0].classList.contains("tapuscrit")) {
        els[j].classList.add("activated");
      }
      if (prec_els.length) {
        if (prec_els[0].classList.contains("barre")) {
          for (var k = 0; k < prec_els.length; k++) {
            prec_els[k].classList.add("activated");
          }
        }
      }

    }

  }

  // steps suivants
  for (var i = val + 1; i < max; i++) {

    /// / var test = document.querySelector('#transcription_natures span[data-step="' + i + '"]');
    // if (!test.classList.contains("current_step")) {

    var els = document.querySelectorAll('#transcription_natures span[data-step="' + i + '"]');
    for (var j = 0; j < els.length; j++) {
      els[j].classList.remove("current_step");
    }


    for (var j = 0; j < els.length; j++) {

      if (els[0].classList.contains("manuscrit")) {
        els[j].classList.remove("visible");
      } else if (els[0].classList.contains("tapuscrit")) {
        els[j].classList.remove("activated");
      }

    }


  }

}



function displayNatures(obj) {
  var main = document.querySelector("main");
  var facsimile = document.querySelector("#facsimile img");

  var zoom = 1;
  var container_transcription = document.querySelector("#transcription_natures");

  imagesLoaded(facsimile, function( instance ) {
    img_width = facsimile.clientWidth;
    container_transcription.style.fontSize = (Math.round( (facsimile.clientWidth / 61) * 100) / 100)+ "px";
  });

  for (var i = 0; i <= obj.length - 1; i++) {
    var new_span = document.createElement("SPAN");

    new_span.className = "step" + obj[i].class;


    // if (obj[i].class == "0") new_span.className += " current_step";
    // console.log(obj[i].nature);
    if (obj[i].nature === "manuscrit") {
        new_span.style.fontSize = (Math.round( (facsimile.clientWidth / 76) * 100) / 100)+ "px";
    }

    new_span.className += " " + obj[i].nature;
    new_span.className += " " + obj[i].style;
    new_span.setAttribute("data-step", obj[i].class);
    new_span.setAttribute("data-intervention", obj[i].intervention);
    new_span.innerHTML = obj[i].contenu;

    new_span.style.top = obj[i].top + "%";
    new_span.style.left = obj[i].left + "%";
    // new_span.style.width = obj[i].width + "%";
    // new_span.style.height = zoom * obj.height + "%";
    new_span.style.height = zoom * obj[i].height + "%";

    new_span.style.transform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.webkitTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.MozTransform = "rotate(" + obj[i].rotate + "deg)";
    new_span.style.msTransform = "rotate(" + obj[i].rotate + "deg)";

    new_span.addEventListener("mouseover", hoverSpanTranscription);
    new_span.addEventListener("mouseleave", hoverSpanTranscription);

    container_transcription.appendChild(new_span);
  }

}

function trouveEquivalents(step) {
  var els = document.querySelectorAll("#container_transcription span[data-step='" + step + "']");
  return els;
}





function hoverSpanTranscription(event) {

  var currentClass = this.getAttribute("data-step");
  var otherEls_tr = document.querySelectorAll('#container_transcription span[data-step="' + currentClass + '"]');
  var otherEls = document.querySelectorAll('#transcription_natures span[data-step="' + currentClass + '"]');
  var alreadyHovered = document.querySelectorAll(".hovered");

  for (var i = 0; i < alreadyHovered.length; i++) {
    alreadyHovered[i].classList.remove("hovered");
  }

  if (event.type === "mouseover") {
    for (var i = 0; i < otherEls_tr.length; i++) {
      otherEls_tr[i].classList.add("hovered");
      otherEls[i].classList.add("hovered");
    }
  }

}




function displayObject(obj) {
  var zoom = 1;
  var div_transcription = document.querySelector("#transcription");

  var new_span = document.createElement("SPAN");
  new_span.innerHTML = obj.contenu;
  new_span.style.left = zoom * obj.leftDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";

  new_span.style.top = zoom * obj.topDiplomatique + "%";
  new_span.style.top = zoom * obj.topDiplomatique + "%";

  div_transcription.appendChild(new_span);
}

function parseData(url, callBack) {
    Papa.parse(url, {
        download: true,
        header: true,
        complete: function(results) {
            callBack(results.data);
        }
    });
}



function ScreenSize(){
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];

    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
    screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}

var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};




/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];




****** DÉCLARATION DE FONCTION ******

function maFonction() {
	console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
	console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
	console.log("vrai");
}

if (condition1) {
	console.log("condition1 : vrai");
} else if (condition2) {
	console.log("condition2 : vrai");
} else {
	console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut", "valeur_attribut");
el.getAttribute("nom_attribut");




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended", "maFonction()");
video.setAttribute("controls", "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
	console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** COORDONNEES D'UN ELEMENT ******
var rect = el.getBoundingClientRect();
var x = rect.left;




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/
