var Encore = require('@symfony/webpack-encore');

Encore
  // directory where compiled assets will be stored
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .addEntry('js/main', './assets/js/main.js')
  .addEntry('js/display-fiche', './assets/js/display-fiche.js')
  .addEntry('js/inventaire', './assets/js/inventaire.js')
  .addEntry('js/display-fiches', './assets/js/display-fiches.js')

  .addStyleEntry('css/template', './assets/css/template.css')
  .addStyleEntry('css/project', './assets/css/project.css')
  .addStyleEntry('css/reticence', './assets/css/reticence.css')

  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning()
  .createSharedEntry('js/vendor', './assets/js/vendor.js')
  .autoProvidejQuery();

module.exports = Encore.getWebpackConfig();
