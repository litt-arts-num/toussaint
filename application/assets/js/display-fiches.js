$('#text-filter').on('keyup', () => {
  filter()
})


const filter = () => {
  setTimeout(function () {
    let medias = Array.from(document.getElementsByClassName('fiche'))
    let text =  document.querySelector('#text-filter').value.toLowerCase()


    medias.forEach(function (media) {
      if (media.getAttribute('data-name').toLowerCase().includes(text) ) {
        media.parentNode.classList.remove('d-none')
      } else {
        media.parentNode.classList.add('d-none')
      }
    })
  }, 100)
}
