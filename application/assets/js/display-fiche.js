import OpenSeadragon from "openseadragon";

$(function() {
  let viewerLeft = createViewer("seadragon-viewer-left", "left")
  let viewerRight = createViewer("seadragon-viewer-right", "right")
})

$(document).keydown(function(e) {
    switch(e.which) {
        case 37:
          let previous = document.getElementById('previous-btn')
          if (previous) {
            window.location = previous.dataset.absolute
          }
          break;

        case 39:
          let next = document.getElementById('next-btn')
          if (next) {
            window.location = next.dataset.absolute
          }
          break;

        default: return;
    }
    e.preventDefault();
});


const createViewer = (id, prefix) => {
  let url = document.getElementById('imageURL').value
  let viewer = new OpenSeadragon.Viewer({
    id: id,
    showNavigator: false,
    showRotationControl: true,
    prefixUrl: 'node_modules/openseadragon/build/openseadragon/images',
    zoomInButton: prefix + '-osd-zoom-in',
    zoomOutButton: prefix + '-osd-zoom-out',
    homeButton: prefix + '-osd-home',
    fullPageButton: prefix + '-osd-full-page',
    rotateLeftButton: prefix + '-osd-left',
    rotateRightButton: prefix + '-osd-right',
    tileSources: {
      type: 'image',
      url: url
    }
  })
  // disable keyboard shortcuts
  viewer.innerTracker.keyHandler = null
  viewer.addHandler('open', function() {
    var viewportBounds = viewer.viewport.getBounds();
    var viewportAspect = viewportBounds.height / viewportBounds.width;
    var newBounds = viewer.world.getHomeBounds();
    newBounds.height = newBounds.width * viewportAspect;
    viewer.viewport.fitBounds(newBounds);
  });

  return viewer
}
